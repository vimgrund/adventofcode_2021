# tag::parseInput[]
def parseInput(file):
    with open(file, 'r') as f:
        lines = []
        while True:
            blub = []
            line = f.readline()
            if line == "":
                break
            for x in line:
                if x == '\n':
                    break
                blub.append(Octo(int(x)))
            lines.append(blub)
        heightmap = OctoMap(lines)
    return heightmap
# end::parseInput[]

class Octo:
    def __init__(self, level):
        self.level = level
    def inc(self, inc = 1):
        self.level += inc
        return self.level

class OctoMap:
    rows = []
    def __init__(self, lines):
        self.rows = lines

    def calculateFlashes(self, cycles):
        flashes = 0
        for i in range(0, cycles):
            self.incrementAll()
            flashes += self.countFlashes()
        return flashes

    def cycleTillFullFlash(self):
        i = 1
        while self.calculateFlashes(1) != 100:
            i += 1
        return i

    def incrementAll(self):
        for y in range(0,len(self.rows)):
            for x in range(0, len(self.rows[0])):
                self.increment(x,y)    

    def increment(self, x, y):
        level = self.rows[y][x].inc()
        if level == 10:
            # increment all neighbors
            for xx in range(x-1, x+2):
                for yy in range(y-1, y+2):
                    if xx >= 0 and xx < len(self.rows[0]) \
                        and yy >= 0 and yy < len(self.rows):
                        self.increment(xx,yy)
                    pass

    def countFlashes(self):
        count = 0
        for row in self.rows:
            for octo in row:
                if octo.level >= 10:
                    octo.level = 0
                    count += 1
        return count

    def print(self):
        for row in self.rows:
            line = ""
            for octo in row:
                if octo.level >= 10:
                    line += "#"
                else:
                    line += str(octo.level)
            print(line, flush=True)


print("----------------Puzzle 1--------------")

# tag::example1[]
test_octomap = parseInput("test.txt")
print("Test input, should be 1656")
blub = test_octomap.calculateFlashes(100)
test_octomap.print()
print(blub)
# end::example1[]

octomap = parseInput("input.txt")
print("Test input, should be 1656")
blub = octomap.calculateFlashes(100)
octomap.print()
print(blub)

print("----------------Puzzle 2--------------")

# tag::example1[]
test_octomap = parseInput("test.txt")
print("Test input, should be 195")
blub = test_octomap.cycleTillFullFlash()
test_octomap.print()
print(blub)
# end::example1[]

octomap = parseInput("input.txt")
print("Test input, should be 1656")
blub = octomap.cycleTillFullFlash()
octomap.print()
print(blub)
