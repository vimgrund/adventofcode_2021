# tag::parseInput[]
def parseInput(file):
    with open(file, 'r') as f:
        lines = []
        while True:
            blub = []
            line = f.readline()
            if line == "":
                break
            for x in line:
                if x == '\n':
                    break
                blub.append(int(x))
            lines.append(blub)
        heightmap = RiskMap(lines)
    return heightmap
# end::parseInput[]

def getMin(*values):
    result = None
    for value in values:
        if result is None or value < result:
            result = value
    return result

class RiskMap:
    rows = []
    def __init__(self, lines):
        self.rows = lines

    def calcPaths(self):
        self.paths = []
        self.lowest = 9999999
        # Path(self, [], self.getPoint(0,0), len(self.rows))
        limit = len(self.rows)
        path =0
        x = 9
        y = 0
        for risks in self.rows[0][1:]:
            path += risks
        for i in range(0, limit):
            path += self.rows[i][limit-1]

        while x != 0:
            path = path - self.rows[y][x]
            path += self.rows[y+1][x-1]
            
            y += 1
            if (y == 8):
                x -= 1
                y = 0
            if path < self.lowest:
                self.lowest = path


        return self.lowest
        

    def getPoint(self,x,y):
        return Point(x,y, self.rows[y][x])

    def getMinRisk(self):
        return self.lowest
        min = None
        for path in self.paths:
            if min == None or min.getRisk() > path.getRisk():
                min = path
            path.print()
        min.print()
        return min.getRisk()

class Path:
    points = []
    def __init__(self, riskmap, origin, point, limit):
        self.risk = None
        self.points = origin.copy()
        self.points.append(point)
        if point.x == limit-1 and point.y == limit-1:
            # riskmap.paths.append(self)
            if riskmap.lowest > self.getRisk():
                riskmap.lowest = self.getRisk()
            # print(f"found path, risk level {self.getRisk()}")
            return
        if point.x+1 < limit: #and not self.contains(point.x+1, point.y):
            Path(riskmap, self.points, riskmap.getPoint(point.x+1, point.y), limit)
        if point.y+1 < limit: # and not self.contains(point.x, point.y+1):
            Path(riskmap, self.points, riskmap.getPoint(point.x, point.y+1), limit)

    def contains(self, x, y):
        for point in self.points:
            if point.x == x and point.y == y:
                return True
        return False

    def getRisk(self):
        if self.risk != None:
            return self.risk
        counter = 0
        for point in self.points[1:]:
            counter += point.r
        self.risk = counter
        return counter

    def print(self):
        limit = self.points[-1].x + 1
        rows = []
        for i in range(0,limit):
            rows.append([' ']*limit)

        for point in self.points:
            rows[point.y][point.x] = str(point.r)
        for row in rows:
            print(row)

class Point:
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def equals(self, point):
        if self.x == point.x and self.y == point.y:
            return True
        return False

print("----------------Puzzle 1--------------")

# tag::example1[]
test_riskmap = parseInput("test.txt")
print("Test input, should be 40")
test_riskmap.calcPaths()

blub = test_riskmap.getMinRisk()
print(blub)
# end::example1[]

riskmap = parseInput("input.txt")
print("Real Input")
riskmap.calcPaths()
blub = riskmap.getMinRisk()
print(blub)
