# tag::parseInput[]
def parseInput(file):
    with open(file, 'r') as f:
        polymer = f.readline()
        if polymer[-1] == '\n':
            polymer = polymer[:-1]
        instructions = []
        while True:
            line = f.readline()
            if line == "":
                break
            if line == "\n":
                continue
            if line[-1] == '\n':
                line = line[:-1]

            blub = line.split('->')
            input = blub[0].strip()
            output = input[0] + blub[1].strip() + input[1]
            instructions.append(Instruction(input, output))
        map = PolymerTemplate(polymer, instructions)
    return map
# end::parseInput[]

def getMax(*values):
    result = None
    for value in values:
        if result is None or value > result:
            result = value
    return result

def getMin(*values):
    result = None
    for value in values:
        if result is None or value < result:
            result = value
    return result

class Instruction:
    def __init__(self, pattern, result):
        self.pattern = pattern
        self.result = result

class PolymerTemplate:
    def __init__(self, initial, instructions):
        self.polymer = initial 
        self.instructions = instructions

    def doSteps(self, steps_count):
        for i in range(0, steps_count):
            self.insert()            
    def doStepsQuick(self, steps_count):
        for i in range(0, steps_count):
            print(i)
            self.insertQuick()            

    def insert(self):
        new = ""
        for i in range(0, len(self.polymer) - 1):
            pair = self.polymer[i] + self.polymer[i+1]
            for ins in self.instructions:
                if ins.pattern == pair:
                    new += ins.result[:-1]
                    break
        self.polymer = new + self.polymer[-1] # append last element
        # print(self.polymer, flush=True)

    def insertQuick(self):
        self.polymer = self.resolf(self.polymer)
        print(f"{len(self.polymer)} - {len(self.instructions)}", flush=True)

    def resolf(self, part):
        new = ""
        if len(part) == 1:
            return part
        for ins in self.instructions:
            if ins.pattern == part:
                return ins.result
        
        half = int(len(part)/2)
        part1 = part[:half]
        part2 = part[half:]
        new = self.resolf(part1)
        new = new[:-1] + self.resolf(part1[-1]+part2[0])
        new = new[:-1] + self.resolf(part2)

        if len(part) < 2000000000:
            self.instructions.append(Instruction(part, new))
        return new


    def getExtrema(self):
        counts = {}
        for elem in self.polymer:
            if elem in counts:
                counts[elem] += 1
            else:
                counts[elem] = 1
        
        min = None
        max = None
        for elem in counts:
            max = getMax(max, counts[elem])
            min = getMin(min, counts[elem])
        # print(counts)
        return max,min

print("----------------Puzzle 1--------------")

# tag::example1[]
test_pt = parseInput("test.txt")
print("Test input, should be \"1749 - 161 = 1588\"")
test_pt.doSteps(10)
most,least = test_pt.getExtrema()
print(f"{most} - {least} = {most-least}")
# end::example1[]

pt = parseInput("input.txt")
print("Real Input")
pt.doSteps(10)
most,least = pt.getExtrema()
print(f"{most} - {least} = {most-least}")


print("----------------Puzzle 2--------------")

# tag::example1[]
test_pt = parseInput("test.txt")
print("Test input, should be \"2192039569602 - 3849876073 = 2188189693529\"")
test_pt.doStepsQuick(40)
most,least = test_pt.getExtrema()
print(f"{most} - {least} = {most-least}")
# end::example1[]

exit()
pt = parseInput("input.txt")
print("Real Input")
pt.doSteps(40)
most,least = pt.getExtrema()
print(f"{most} - {least} = {most-least}")
