def parseInput(file):
    with open(file, 'r') as f:
        numbers = []
        while True:
            blub = []
            line = f.readline()
            if line == "":
                break
            if line[-1] == '\n':
                line = line[:-1]
            numbers.append(Number(line))
    return numbers

def addAllNumbers(numbers):
    result = numbers[0]
    for number in numbers[1:]:
        result.add(number)
    return result

def reallyAddAllNumbers(numbers):
    result = []
    for i in range(len(numbers)):
        for y in range(len(numbers)):
            if i == y:
                continue
            blub = Number(numbers[i].getString())
            blub.add(Number(numbers[y].getString()))
            result.append(blub)
    return result

def findHighestMagnitude(blub):
    highestMag = 0
    for number in blub:
        number.reduce()
        mag = number.getMagnitude()
        if mag > highestMag:
            highestMag = mag
    return highestMag

class Digit:
    def __init__(self, number, depth):
        self.number = number
        self.depth = depth
class Spacer:
    def __init__(self, depth):
        self.depth = depth
class Number:
    def __init__(self, str, depth=0):
        self.digits = []
        self.str = str
        for digit in str:
            if digit == '[':
                depth += 1
            elif digit == ']':
                depth -= 1
            elif digit == ',':
                self.digits.append(Spacer(depth))
            else:
                self.digits.append(Digit(int(digit), depth))

    def add(self, number):
        for digit in self.digits:
            digit.depth += 1
        self.digits.append(Spacer(1))
        for digit in number.digits:
            digit.depth += 1
            self.digits.append(digit)
        self.reduce()

    def reduce(self):
        goOn = True
        while goOn:
            goOn = self.explode()
            goOn = self.split() or goOn
        self.str = self.getString()
    def explode(self):
        toggle = False
        exploded = False
        remove_digits = []
        for i in range(0, len(self.digits)):
            digit = self.digits[i]
            if digit.depth == 5:
                if type(digit) is Spacer:
                    remove_digits.append(digit)
                    continue
                if toggle:
                    if i < len(self.digits) - 1:
                        x = i+1
                        while type(self.digits[x]) is Spacer:
                            x += 1
                        self.digits[x].number += digit.number
                    digit.number = 0
                    digit.depth -= 1
                elif i > 0:
                    x = i-1
                    while type(self.digits[x]) is Spacer:
                        x -= 1
                    self.digits[x].number += digit.number
                toggle = not toggle
                if toggle:
                    remove_digits.append(digit)
            elif digit.depth > 5:
                print("ERROR")
                exit()
        for digit in remove_digits:
            exploded = True
            self.digits.remove(digit)
        return exploded

    def split(self):
        for digit in self.digits:
            if type(digit) is Spacer:
                continue
            if digit.number > 9:
                index = self.digits.index(digit)
                left = Digit(int(digit.number/2), digit.depth + 1)
                right = Digit(int(digit.number/2) + digit.number %
                              2, digit.depth + 1)
                spacer = Spacer(digit.depth + 1)
                new = self.digits[:index]
                new.append(left)
                new.append(spacer)
                new.append(right)
                new += self.digits[index+1:]
                self.digits = new
                return True
        return False

    def getMagnitude(self):
        depth = 4
        digits = self.digits.copy()
        while depth > 0:
            i = 0
            while True:
                digit = digits[i]
                if digit.depth == depth:
                    result = digit.number * 3
                    result += digits[i+2].number *2
                    result = Digit(result, depth-1)
                    new = digits[:i]
                    new.append(result)
                    new += digits[i+3:]
                    digits = new
                    i += 1
                i += 1
                if i >= len(digits):
                    break
            depth -= 1
        return digits[0].number
    def getString(self):
        output = ""
        depth = 0
        for digit in self.digits:
            while digit.depth > depth:
                output += '['
                depth += 1
            while digit.depth < depth:
                output += ']'
                depth -= 1
            if type(digit) is Spacer:
                output += ','
            else:
                output += str(digit.number)
        while depth > 0:
            output += ']'
            depth -= 1
        return output
    def print(self):
        print(self.getString(), flush=True)




# blub = Number("[[[[4,3],4],4],[7,[[8,4],9]]]")
# blub.print()

# blub2 = Number("[1,1]")
# blub.add(blub2)
# blub.print()
# blub.explode()
# blub.print()
# blub.split()
# blub.print()
# blub.split()
# blub.print()
# blub.explode()
# blub.print()
# exit()

# blub.print()
# blub.reduce()
# blub.print()



print("------------- Puzzle 1 --------------")

test_numbers = parseInput("test.txt")
blub = addAllNumbers(test_numbers)
print("should be \n[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")
blub.print()


test_numbers = parseInput("test2.txt")
blub = addAllNumbers(test_numbers)
print("should be \n[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]")
blub.print()

print("Mangitude shoud be \n4140")
print(f"{blub.getMagnitude()}")

numbers = parseInput("input.txt")
blub = addAllNumbers(numbers)
print("Real Input")
blub.print()

print("Mangitude")
print(f"{blub.getMagnitude()}")


print("------------- Puzzle 2 --------------")

test_numbers = parseInput("test2.txt")
blub = reallyAddAllNumbers(test_numbers)
highestMag = findHighestMagnitude(blub)
print("Highest Mag, shoud be\n3993")
print(highestMag)

numbers = parseInput("input.txt")
blub = reallyAddAllNumbers(numbers)
highestMag = findHighestMagnitude(blub)
print("Real Input")
print(highestMag)


# blub = Number("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]")
# blub.add(Number("[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"))
# blub.print()
# blub.reduce()
# blub.print()


# blub.add(Number("[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[7,[5,[[3,8],[1,4]]]]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[[2,[2,2]],[8,[8,1]]]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[2,9]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[1,[[[9,3],9],[[9,0],[0,7]]]]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[[[5,[7,4]],7],1]"))
# blub.reduce()
# blub.print()
# blub.add(Number("[[[[4,2],2],6],[8,7]]"))
# blub.reduce()
# blub.print()

# exit()
