# tag::parseInput[]
def parseInput(string):
    blub = string.split(':') [1]
    blub = blub.split(',')
    xMin,xMax = blub[0].split('..')
    xMax = int(xMax)
    xMin = int(xMin.split('=')[1])
    yMin,yMax = blub[1].split('..')
    yMax = int(yMax)
    yMin = int(yMin.split('=')[1])
    return Target(xMin, xMax, yMin, yMax)
# end::parseInput[]


class Probe:
    def __init__(self, xVel, yVel):
        self.x = 0
        self.y = 0
        self.high = 0
        self.xVel = xVel
        self.yVel = yVel

    def doStep(self):
        self.x += self.xVel
        self.y += self.yVel
        if self.high < self.y:
            self.high = self.y
        if self.xVel != 0:
            self.xVel -= 1
        self.yVel -= 1

    def isInRange(self, target):
        if self.x > target.xMax or self.y < target.yMin:
            return False
        if self.xVel == 0 and self.x < target.xMin:
            return False
        return True

    def isHit(self, target):
        if self.x <= target.xMax and self.x >= target.xMin \
                and self.y <= target.yMax and self.y >= target.yMin:
            return True
        return False


class Target:
    def __init__(self, xMin, xMax, yMin, yMax):
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax
        self.hitter = []

    def shoot(self):
        highest = 0
        highestX = 0
        highestY = 0
        for x in range(1, self.xMax+1):
            for y in range(self.yMin, 0-self.yMin):
                probe = Probe(x, y)
                while probe.isInRange(self):
                    probe.doStep()
                    if probe.isHit(self):
                        self.hitter.append(f"{x},{y}")
                        if highest < probe.high:
                            highest = probe.high
                            highestX = x
                            highestY = y
                        break
        print(f"high at x={highestX}, y={highestY} = > {highest} ")
        print(f"number of Hits = {len(self.hitter)}")
    

test_target = parseInput("target area: x=20..30, y=-10..-5")
test_target.shoot()

target = parseInput("target area: x=48..70, y=-189..-148")
target.shoot()
