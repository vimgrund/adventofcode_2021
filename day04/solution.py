# tag::parseInput[]
def parseInput(file):
    """parse input file to produce a int[] and a list of Bingo-Object"""
    queue = []
    boards = []
    with open(file, 'r') as f:
        # read queue
        queue = [int(x) for x in f.readline().split(',')]

        i = 0
        board_rows = []
        while i < 5:
            line = f.readline()
            if line == '\n':
                continue
            if line == "":
                break
            i += 1
            parts = [int(x) for x in line.split()]
            board_rows.append(parts)
            if i == 5:
                boards.append(Board(board_rows))
                i = 0
                board_rows = []
    return queue, boards
# end::parseInput[]

class Board:
    """
    5x5 Bingo Matrix
    Stores a grid of values and a checked flag
    """
    rows = []

    def __init__(self, rows):
        self.rows = [] 
        for row in rows:
            elements = []
            for number in row:
                element = {}
                element['checked'] = False
                element['value'] = number
                elements.append(element)
            self.rows.append(elements)

    def check(self, number):
        """
        Checks for a number in the Bingo Matrix and flags it if applicable
        if bingo is hit, the sum of all unchecked values is multiplied by the current number
        else 0 is returned
        """
        for row in self.rows:
            for value in row:
                if value['value'] == number:
                    value['checked'] = True
                    if self.isRowChecked(self.rows.index(row)) \
                        or self.isColumnChecked(row.index(value)):
                        return self.sumUnchecked() * number
                    return 0 # break, there are no dublicates
        return 0
    
    def isRowChecked(self, row):
        """ checks a specific row for completeness """
        for element in self.rows[row]:
            if not element['checked']:
                return False
        return True

    def isColumnChecked(self, column):
        """ checks a specific column for completeness """
        for i in range(0, len(self.rows)):
            if not self.rows[i][column]['checked']:
                return False
        return True

    def sumUnchecked(self):
        """ sum of all unchecked values """
        sum = 0
        for row in self.rows:
            for value in row:
                if not value['checked']:
                    sum += value['value']
        return sum
    


print("----------------Puzzle 1--------------")

# tag::example1[]
test_queue, test_boards = parseInput("test.txt")
print("Test input, should be 4512")
bingo = False
for number in test_queue:
    for board in test_boards:
        result = board.check(number)
        if result != 0:
            print(f"bingo - {result}")
            bingo = True
            break
    if bingo:
        break
# end::example1[]

queue, boards = parseInput("input.txt")
print("Real Input")
bingo = False
for number in queue:
    for board in boards:
        result = board.check(number)
        if result != 0:
            print(f"bingo - {result}")
            bingo = True
            break
    if bingo:
        break

print("----------------Puzzle 2--------------")
# tag::example2[]
print("Test input, should be 1924")
worst_board = 0
result = 0
for board in test_boards:
    cycles = 0
    for number in test_queue:
        cycles += 1
        bingo = board.check(number)
        if bingo != 0: 
            if cycles > worst_board:
                worst_board = cycles
                result = bingo
            break
print(f"worst bingo: {result}")
# end::example2[]

print("Real Input")
worst_board = 0
result = 0
for board in boards:
    cycles = 0
    for number in queue:
        cycles += 1
        bingo = board.check(number)
        if bingo != 0: 
            if cycles > worst_board:
                worst_board = cycles
                result = bingo
            break
print(f"worst bingo: {result}")
