# tag::parseInput[]
def parseInput(file):
    """parse input file to produce a int[]"""
    initialStates = []
    with open(file, 'r') as f:
        initialStates = [int(x) for x in f.readline().split(',')]
        fishPod = LanternFishBrothel(initialStates)
    return fishPod
# end::parseInput[]

class LanternFishBrothel:
    """
    Spawn Area for Lantern fish... Here is, where the secks happens
    """

    def __init__(self, stock):
        self.stock = [0] * 9
        for fish in stock:
            self.stock[fish] += 1

    def cycleOneDay(self):
        """
        create new Stock array, all entries go to field i-1
        field 0 is written to new field 6 (in addition to the prev. field 7)
        and to field 8 (newborns)
        """
        newStock = [0] * 9
        for i in range(0, len(self.stock)):
            if i == 0:
                newStock[6] = self.stock[0]
                newStock[8] = self.stock[0]
            else:
                newStock[i-1] += self.stock[i]
        self.stock = newStock
    
    def howMuchisThereFish(self):
        counter = 0
        for fish in self.stock:
            counter += fish
        return counter


print("----------------Puzzle 1--------------")

# tag::example1[]
test_fishpod = parseInput("test.txt")
print("Test input, should be 5934")
for i in range(0,80):
    test_fishpod.cycleOneDay()
blub = test_fishpod.howMuchisThereFish()
print(blub)
# end::example1[]

fishpod = parseInput("input.txt")
print("Real Input")
for i in range(0,80):
    fishpod.cycleOneDay()
blub = fishpod.howMuchisThereFish()
print(blub)


print("----------------Puzzle 2--------------")
# tag::example2[]
test_fishpod = parseInput("test.txt")
print("Test input, should be 26984457539")
for i in range(0,256):
    test_fishpod.cycleOneDay()
blub = test_fishpod.howMuchisThereFish()
print(blub)
# end::example2[]

fishpod = parseInput("input.txt")
print("Real Input")
for i in range(0,256):
    fishpod.cycleOneDay()
blub = fishpod.howMuchisThereFish()
print(blub)
