# tag::parseInput[]
def parseInput(file):
    with open(file, 'r') as f:
        connections = []
        while True:
            line = f.readline()
            if line == "":
                break
            if line[-1] == '\n':
                line = line[:-1] 
            blub = line.split('-')
            connections.append(Connection(blub[0], blub[1]))
        heightmap = CaveMap(connections)
    return heightmap
# end::parseInput[]

class Connection:
    def __init__(self, caveA, caveB):
        self.caveA = caveA
        self.caveB = caveB

class Cave:
    def __init__(self, name):
        self.name = name
        self.neighbors = []

    def setNeighbor(self, neighbor):
        self.neighbors.append(neighbor)

class CaveMap:
    connections = []
    def __init__(self, connections):
        self.connections = connections
        self.caves = []
        self.paths = []
        cavenames = []
        for con in connections:
            caveA = None
            caveB = None
            if con.caveA not in cavenames:
                caveA = Cave(con.caveA)
                self.caves.append(caveA)
                cavenames.append(caveA.name)
            else:
                caveA = self.caves[cavenames.index(con.caveA)]
            if con.caveB not in cavenames:
                caveB = Cave(con.caveB)
                self.caves.append(caveB)
                cavenames.append(caveB.name)
            else:
                caveB = self.caves[cavenames.index(con.caveB)]
            caveA.setNeighbor(caveB)
            caveB.setNeighbor(caveA)

    def getCave(self, name):
        for cave in self.caves:
            if cave.name == name:
                return cave
        print(f"Error: cave {name} not found")
        exit()

    def calculatePath(self):
        self.path = []
        start = self.getCave('start')
        path = ['start']
        for neighbor in start.neighbors:
            self.addPath(path.copy(), neighbor)
        return len(self.paths)
    def addPath(self, path, cave):
        if isOnlyLower(cave.name):
            if cave.name in path:
                return
        path.append(cave.name)
        if cave.name == 'end':
            self.paths.append(path)
            return
        for neighbor in cave.neighbors:
            self.addPath(path.copy(), neighbor)

    def calculatePath2(self):
        self.paths = []
        start = self.getCave('start')
        path = ['start']
        for neighbor in start.neighbors:
            self.addPath2(path.copy(), neighbor)
        return len(self.paths)
    def addPath2(self, path, cave):
        if isOnlyLower(cave.name):
            if cave.name in path :
                if 'hasTwo' in path:
                    return
                path.append('hasTwo')
        path.append(cave.name)
        if cave.name == 'end':
            self.paths.append(path)
            return
        for neighbor in cave.neighbors:
            if neighbor.name == 'start':
                continue
            self.addPath2(path.copy(), neighbor)

    def print(self):
        for cave in self.caves:
            print(cave.name, flush=True)

def isOnlyLower(str):
    for char in str:
        if not char.islower():
            return False
    return True

print("----------------Puzzle 1--------------")

# tag::example1[]
test1_cavemap = parseInput("test1.txt")
print("Test input, should be 10")
blub = test1_cavemap.calculatePath()
print(blub)
# end::example1[]

test2_cavemap = parseInput("test2.txt")
print("Test input, should be 19")
blub = test2_cavemap.calculatePath()
print(blub)

test3_cavemap = parseInput("test3.txt")
print("Test input, should be 226")
blub = test3_cavemap.calculatePath()
print(blub)

cavemap = parseInput("input.txt")
print("Real input")
blub = cavemap.calculatePath()
print(blub)


print("----------------Puzzle 2--------------")

# tag::example2[]
print("Test input, should be 36")
blub = test1_cavemap.calculatePath2()
print(blub)
# end::example2[]

print("Test input, should be 103")
blub = test2_cavemap.calculatePath2()
print(blub)

print("Test input, should be 3509")
blub = test3_cavemap.calculatePath2()
print(blub)

print("Real input")
blub = cavemap.calculatePath2()
print(blub)
