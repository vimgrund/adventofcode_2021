# tag::parseInput[]
def parseInput(file):
    with open(file, 'r') as f:
        lines = []
        while True:
            blub = []
            line = f.readline()
            if line == "":
                break
            for x in line:
                if x == '\n':
                    break
                blub.append(int(x))
            lines.append(blub)
        heightmap = HeightMap(lines)
    return heightmap
# end::parseInput[]

class HeightMap:
    rows = []
    def __init__(self, lines):
        self.rows = lines

    def calculateRiskLevel(self):
        level = 0
        rowmax = len(self.rows) -1
        columnmax = len(self.rows[0]) -1
        y = 0
        x = 0
        for y in range(0, len(self.rows)):
            for x in range(0, len(self.rows[y])):
                isLow = True
                if y != 0 and self.rows[y][x] >= self.rows[y-1][x]:
                    isLow = False
                if x != 0 and self.rows[y][x] >= self.rows[y][x-1]:
                    isLow = False
                if y != rowmax and self.rows[y][x] >= self.rows[y+1][x]:
                    isLow = False
                if x != columnmax and self.rows[y][x] >= self.rows[y][x+1]:
                    isLow = False
                if isLow:
                    level += self.rows[y][x] + 1
                x += 1
            y += 1
        return level

    def calculateTop3Basins(self):
        basins = []
        rowmax = len(self.rows) -1
        y = 0
        x = 0
        for y in range(0, len(self.rows)):
            columnmax = len(self.rows[0]) -1
            for x in range(0, len(self.rows[y])):
                isLow = True
                if self.rows[y][x] >= 9:
                    isLow = False
                elif y != 0 and self.rows[y][x] > self.rows[y-1][x]:
                    isLow = False
                elif x != 0 and self.rows[y][x] > self.rows[y][x-1]:
                    isLow = False
                elif y != rowmax and self.rows[y][x] > self.rows[y+1][x]:
                    isLow = False
                elif x != columnmax and self.rows[y][x] > self.rows[y][x+1]:
                    isLow = False
                if isLow:
                    # calculate basin
                    basins.append(Basin(self,Point(x,y)))
                    
                x += 1
            y += 1
        basins.sort(key=lambda x: x.getSize(), reverse=True)   # sort
        a,b,c = [x.getSize() for x in basins[:3]]          # take top 3
        return a*b*c
    def print(self):
        for row in self.rows:
            line = ""
            for digit in row:
                if digit == 9:
                    line += "."
                elif digit == 10:
                    line += "#"
                else:
                    line += " "
            print(line, flush=True)

class Basin:
    def __init__(self,heightmap, point):
        self.points =[]
        self.origin = point
        self.points.append(self.origin)
        self.heightmap = heightmap
        # self.heightmap.rows[point.y][point.x]=10
        # self.heightmap.print()
        # print("-------------------------", flush=True)
        # walk left and right
        i = point.x
        while i >= 0 and self.heightmap.rows[point.y][i] != 9:
            self.addPoint_horizontal(Point(i,point.y))
            i -= 1
        i = point.x
        while i < len(self.heightmap.rows[0]) and self.heightmap.rows[point.y][i] != 9:
            self.addPoint_horizontal(Point(i,point.y))
            i += 1
        i = point.y
        # walk up and down
        while i >= 0 and self.heightmap.rows[i][point.x] != 9:
            self.addPoint_vertical(Point(point.x,i))
            i -= 1
        i = point.y
        while i < len(self.heightmap.rows) and self.heightmap.rows[i][point.x] != 9:
            self.addPoint_vertical(Point(point.x,i))
            i += 1

    def getSize(self):
        return len(self.points)

    def addPoint_horizontal(self, point):
        skip = False
        for test in self.points:
            if test.equals(point):
                skip = True
                break
        if skip == False:
            self.points.append(point)
            # self.heightmap.rows[point.y][point.x]=10
            # self.heightmap.print()
            # print("-------------------------", flush=True)
            i = point.y
            # walk up and down
            while i >= 0 and self.heightmap.rows[i][point.x] != 9:
                self.addPoint_vertical(Point(point.x,i))
                i -= 1
            i = point.y
            while i < len(self.heightmap.rows) and self.heightmap.rows[i][point.x] != 9:
                self.addPoint_vertical(Point(point.x,i))
                i += 1


    def addPoint_vertical(self, point):
        skip = False
        for test in self.points:
            if test.equals(point):
                skip = True
                break
        if skip == False:
            self.points.append(point)
            # self.heightmap.rows[point.y][point.x]=10
            # self.heightmap.print()
            # print("-------------------------", flush=True)
            i = point.x
            while i >= 0 and self.heightmap.rows[point.y][i] != 9:
                self.addPoint_horizontal(Point(i, point.y))
                i -= 1
            i = point.x
            while i < len(self.heightmap.rows[0]) and self.heightmap.rows[point.y][i] != 9:
                self.addPoint_horizontal(Point(i, point.y))
                i += 1



class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def equals(self, point):
        if self.x == point.x and self.y == point.y:
            return True
        return False

print("----------------Puzzle 1--------------")

# tag::example1[]
test_heightmap = parseInput("test.txt")
print("Test input, should be 15")
blub = test_heightmap.calculateRiskLevel()
print(blub)
# end::example1[]

heightmap = parseInput("input.txt")
print("Real Input")
blub = heightmap.calculateRiskLevel()
print(blub)


print("----------------Puzzle 2--------------")

# tag::example2[]
print("Test input, should be 1134")
blub = test_heightmap.calculateTop3Basins()
#test_heightmap.print()
print(blub)
# end::example2[]

heightmap = parseInput("input.txt")
print("Real Input")
blub = heightmap.calculateTop3Basins()
#heightmap.print()
print(blub)




