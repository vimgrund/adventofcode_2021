# tag::parseInput[]
def parseInput(file):
    output = []
    with open(file, 'r') as f:
        while True:
            blub = f.readline()
            if blub == "":
                break
            if blub[-1] == '\n': # get rid of linebreak
                blub = blub[:-1]
            output.append(Line(blub))
    return output
# end::parseInput[]

# tag::validate[]
openBraces = '([{<'
closeBraces = ')]}>'
class Line:
    def __init__(self, pattern):
        self.pattern = pattern
    def validate(self):
        self.stack = []
        result = 0
        for char in self.pattern:
            if char in openBraces:      # openBraces = '([{<'
                self.stack.append(char)
            elif char in closeBraces:   # closeBraces = ')]}>'
                lastopen = self.stack[-1]
                if closeBraces.index(char) == openBraces.index(lastopen):
                    # matches, remove from stack
                    self.stack = self.stack[:-1]
                else:
                    if char == ")":
                        result = 3
                    elif char == "]":
                        result = 57
                    elif char == "}":
                        result = 1197
                    elif char == ">":
                        result = 25137
                    return result
        return result
# end::validate[]

# tag::complete[]
    def complete(self):
        cost = 0
        while(len(self.stack) != 0):
            char = self.stack[-1]
            if char == "(":
                result = 1
            elif char == "[":
                result = 2
            elif char == "{":
                result = 3
            elif char == "<":
                result = 4
            cost = cost * 5 + result

            self.stack = self.stack[:-1]
        return cost
# end::complete[]


print("----------------Puzzle 1--------------")

# tag::example1[]
test_lines = parseInput("test.txt")
print("Test input, should be 26397")
blub = 0
for line in test_lines:
    blub += line.validate()
print(blub)
# end::example1[]

lines = parseInput("input.txt")
print("Real Input")
blub = 0
for line in lines:
    blub += line.validate()
print(blub)


print("----------------Puzzle 2--------------")

# tag::example2[]
test_lines = parseInput("test.txt")
print("Test input, should be 288957")
costs = []
for line in test_lines:
    if 0 == line.validate():
        costs.append(line.complete())
costs.sort()
middleindex = int((len(costs)-1)/2)
blub = costs[middleindex]
print(blub)
# end::example2[]

lines = parseInput("input.txt")
print("Real Input")
costs = []
for line in lines:
    if 0 == line.validate():
        costs.append(line.complete())
costs.sort()
middleindex = int((len(costs)-1)/2)
blub = costs[middleindex]
print(blub)
