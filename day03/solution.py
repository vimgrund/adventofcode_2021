# tag::parseInput[]
def parseInput(file):
    """parse input file to list of bit-Array"""
    result = []
    with open(file, 'r') as f:
        for line in f:
            bitarray = []
            for bit in line:
                if bit != '\n': # get rid of linebreaks
                    bitarray.append(int(bit))
            result.append(bitarray)
    return result
# end::parseInput[]

# tag::calculatePowerConsumtion[]
def calculatePowerConsumtion(inputs):
    gamma = 0
    epsilon = 0
    for i in range(0, len(inputs[0])):
        count_zero = 0
        count_one = 0
        for input in inputs:
            if input[i] == 0:
                count_zero += 1
            else:
                count_one += 1
        # Bitshift for gamma and epsilon, add new bit in the end
        gamma = (gamma << 1) | (count_zero < count_one) 
        epsilon = (epsilon << 1) | (count_zero > count_one)
    return gamma, epsilon
# end::calculatePowerConsumtion[]

# tag::calculateLifeSupport[]
def calculateLifeSupport(inputs):
    oxygen = 0
    scrubber = 0

    rows_of_major = inputs.copy()
    i = 0
    while len(rows_of_major) != 1:
        count_zero = 0
        count_one = 0
        for input in rows_of_major:
            if input[i] == 0:
                count_zero += 1
            else:
                count_one += 1
        rows_of_major = filterInput(rows_of_major, count_one<count_zero, i)
        i += 1

    rows_of_minor = inputs.copy()
    i = 0
    while len(rows_of_minor) != 1:
        count_zero = 0
        count_one = 0
        for input in rows_of_minor:
            if input[i] == 0:
                count_zero += 1
            else:
                count_one += 1
        rows_of_minor = filterInput(rows_of_minor, not count_one<count_zero, i)
        i += 1

    for bit in rows_of_major[0]:
        oxygen = (oxygen << 1) | bit

    for bit in rows_of_minor[0]:
        scrubber = (scrubber << 1) | bit

    return oxygen, scrubber

def filterInput(inputs, deleteOnes, index):
    result = []
    for input in inputs:
        if input[index] == 0 and deleteOnes \
            or input[index] == 1 and not deleteOnes:
            result.append(input)
    return result
# end::calculateLifeSupport[]

print("----------------Puzzle 1--------------")

# tag::example1[]
test_values = parseInput("test.txt")
print("Test input, should be \"gamma: 22, epsilon: 9 => Product = 198\"")
gamma, epsilon = calculatePowerConsumtion(test_values)
print(f"gamma: {gamma}, eplison: {epsilon} => Product = {gamma*epsilon}")
# end::example1[]

values = parseInput("input.txt")
print("Real input")
gamma, epsilon = calculatePowerConsumtion(values)
print(f"gamma: {gamma}, eplison: {epsilon} => Product = {gamma*epsilon}")

print("----------------Puzzle 2--------------")

# tag::example2[]
print("Test input, should be \"oxygen: 23, scrubber: 10 => Product = 230\"")
oxygen, scrubber = calculateLifeSupport(test_values)
print(f"oxygen: {oxygen}, scrubber: {scrubber} => Product = {oxygen*scrubber}")
# end::example2[]

print("Real input")
oxygen, scrubber = calculateLifeSupport(values)
print(f"oxygen: {oxygen}, scrubber: {scrubber} => Product = {oxygen*scrubber}")