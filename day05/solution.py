# tag::parseInput[]
def parseInput(file):
    """parse input file to produce a list of Line-Objects and a heatmap with suitable dimensions"""
    with open(file, 'r') as f:
        lines = []
        dimensions = 0
        while True:
            line = f.readline()
            if line == "":
                break
            coordinates = line.split('->')
            x1 = int(coordinates[0].split(',')[0])
            y1 = int(coordinates[0].split(',')[1])
            x2 = int(coordinates[1].split(',')[0])
            y2 = int(coordinates[1].split(',')[1])
            lines.append(Line(x1,y1,x2,y2))
            dimensions = getMax(dimensions, x1, y1, x2, y2)
        heatmap = HeatMap(dimensions + 1)
    return lines, heatmap
# end::parseInput[]

def getMax(*values):
    result = None
    for value in values:
        if result is None or value > result:
            result = value
    return result

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Line:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
    
    def isStraight(self):
        if self.x1 == self.x2 or self.y1 == self.y2:
            return True
        return False

    def is45Degree(self):
        if self.x1 - self.x2 == self.y1 - self.y2 \
            or self.x1 - self.x2 == self.y2 - self.y1:
            return True
        return False

    def getPoints(self):
        points = []
        x = self.x1
        y = self.y1
        points.append(Point(x,y))
        while x != self.x2 or y != self.y2:
            if x > self.x2:
                x -= 1
            elif x < self.x2:
                x += 1
            if y > self.y2:
                y -= 1
            elif y < self.y2:
                y += 1
            points.append(Point(x,y))
        return points

class HeatMap:
    def __init__(self, size):
        self.rows = []
        for x in range(0,size):
            line = []
            for y in range(0,size):
                line.append(0)
            self.rows.append(line)

    def visualPrint(self):
        for line in self.rows:
            output = ""
            for value in line:
                if value == 0:
                    output = output + '.'
                else:
                    output = output + str(value)
            print(output)

    def addLine(self, line):
        points = line.getPoints()
        for point in points:
            self.addPoint(point)

    def addPoint(self, point):
        line = self.rows[point.y]
        line[point.x] += 1

    def countThresholdViolations(self, threshold):
        count = 0
        for line in self.rows:
            for value in line:
                if value >= threshold:
                    count += 1
        return count




print("----------------Puzzle 1--------------")

# tag::example1[]
test_lines, test_heatmap = parseInput("test.txt")
print("Test input, should be 5")
for line in test_lines:
    if line.isStraight():
        test_heatmap.addLine(line)
test_heatmap.visualPrint()
blub = test_heatmap.countThresholdViolations(2)
print(blub)
# end::example1[]

lines, heatmap = parseInput("input.txt")
print("Real Input")
for line in lines:
    if line.isStraight():
        heatmap.addLine(line)
# your terminal cannot handle this
#heatmap.visualPrint()
blub = heatmap.countThresholdViolations(2)
print(blub)



print("----------------Puzzle 2--------------")

# tag::example2[]
print("Test input, should be 12")
for line in test_lines:
    if line.is45Degree():
        test_heatmap.addLine(line)
test_heatmap.visualPrint()
blub = test_heatmap.countThresholdViolations(2)
print(blub)
# end::example2[]


print("Real Input")
for line in lines:
    if line.is45Degree():
        heatmap.addLine(line)
# your terminal cannot handle this
#heatmap.visualPrint()
blub = heatmap.countThresholdViolations(2)
print(blub)
