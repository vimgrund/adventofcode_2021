import json

def hex2bin(hexstr):
    mapping = {
        "0": "0000",
        "1": "0001",
        "2": "0010",
        "3": "0011",
        "4": "0100",
        "5": "0101",
        "6": "0110",
        "7": "0111",
        "8": "1000",
        "9": "1001",
        "A": "1010",
        "B": "1011",
        "C": "1100",
        "D": "1101",
        "E": "1110",
        "F": "1111"
    }
    binstring = ""
    for x in hexstr:
        binstring += mapping[x]
    return binstring

def bin2int(binstr):
    result = 0
    for bit in binstr:
        result = (result << 1) | (bit == '1')
    return result
 
def createPackage(hexstring):
    encoded = hex2bin(hexstring)
    blub, rest = createPackageEncoded(encoded)
    return blub

def createPackageEncoded(encoded):
    blub = Package()
    rest = blub.create(encoded)
    return blub, rest

def createJson(hexstring):
    encoded = hex2bin(hexstring)
    blub, result = createJsonEncoded(encoded)
    return blub

def createJsonEncoded(encoded):
    blub = {}
    blub['version'] = bin2int(encoded[:3])
    blub['typeID'] = bin2int(encoded[3:6])
    blub['packages'] = []
    rest = encoded[6:]
    result = ''
    if blub['typeID'] == 4:
        while True:
            blub['packages'].append(rest[1:5])
            if rest[0] == '0':
                result = rest[5:]
                break
            rest = rest[5:]
        integer = 0
        for package in blub['packages']:
                integer = (integer << 4) | bin2int(package)
        # blub['result'] = integer
        # return encoded[i+5:]
    elif encoded[6] == '0':
        size = bin2int(encoded[7:22])
        blub['size'] = size
        rest = encoded[22:22+size]
        # result = encoded[i+size:]
        blub['packages'] = createJsonPackages(rest)
    elif encoded[6] == '1':
        rest = encoded[18:18+bin2int(encoded[7:18])]
        # result = encoded[i+size:]
        blub['packages'] = createJsonPackages(rest)

    # if result == 'xDEADBEAF':
    #     print("blub")
    return blub, result
'11000110000000001000010110000000001010110011011100001'
def createJsonPackages(json):
    blub = []
    rest = json
    while len(rest) > 4:
        package,rest = createJsonEncoded(rest)
        blub.append(package)
    return blub

class Package:
    def __init__(self):
        self.version = 0
        self.typeID = 0
        self.packages = []

    def create(self, encoded):
        self.version = bin2int(encoded[:3])
        self.typeID= bin2int(encoded[3:6])
        i = 6
        if self.typeID == 4:
            while True:
                self.packages.append(encoded[i+1:i+5])
                if encoded[i] == '0':
                    break
                i += 5
            return encoded[i+5:]
        # elif length encoding == 0
        size = 0
        if encoded[i] == '0':
            size = bin2int(encoded[i+1:i+16])
            i += 16
            rest = encoded[i:i+size]
            result = encoded[i+size:]
            while len(rest):
                package, rest = createPackageEncoded(rest)
                self.packages.append(package)
            return result
        # elif lenght encoding == 1
        size = bin2int(encoded[i+1:i+12])
        i += 12
        rest = encoded[i:]
        for _ in range(0,size):
            package, rest = createPackageEncoded(rest)
            self.packages.append(package)
        return rest


    def calc(self):
        if self.typeID == 0: # sum
            result = 0
            for package in self.packages:
                result += package.calc()
            return result
        elif self.typeID == 1: # product
            result = 1
            for package in self.packages:
                result *= package.calc()
            return result
        elif self.typeID == 2: # minimum
            result = None
            for package in self.packages:
                calc = package.calc()
                if result == None or result > calc:
                    result = calc
            return result
        elif self.typeID == 3: # maximum
            result = None
            for package in self.packages:
                calc = package.calc()
                if result == None or result < calc:
                    result = calc
            return result
        elif self.typeID == 4: # bin2int
            integer = 0
            for package in self.packages:
                integer = (integer << 4) | bin2int(package)
            return integer
        elif self.typeID == 5: # greater than
            result = None
            if self.packages[0].calc() > self.packages[1].calc():
                return 1
            return 0
        elif self.typeID == 6: # less than
            result = None
            if self.packages[0].calc() < self.packages[1].calc():
                return 1
            return 0
        elif self.typeID == 7: # equals
            result = None
            if self.packages[0].calc() == self.packages[1].calc():
                return 1
            return 0
        else:
            print(f"ERROR: typeID {self.typeID}")
            exit()

    def dump(self):
        dict = {}
        dict['version'] = self.version
        dict['typeID'] = self.typeID
        dict['packages'] = []
        if self.typeID == 4:
            for package in self.packages:
                dict['packages'].append(package)
        else:
            for package in self.packages:
                dict['packages'].append(package.dump())
        return dict

    def versionSum(self):
        sum = self.version
        if self.typeID == 4:
            return sum
        for package in self.packages:
            sum += package.versionSum()
        return sum

teststring = "D2FE28"
print(f"Test {teststring} \n110100101111111000101000 - expected")
print(hex2bin(teststring))

print("Test Calculate, should be 2021")
blub = createPackage(teststring)
print(blub.calc())
print(blub.dump())

blub2 = createJson(teststring)
print(blub2)


teststring = "38006F45291200"
print(f"Test {teststring} \n00111000000000000110111101000101001010010001001000000000 - expected")
print(hex2bin(teststring))
blub = createPackage(teststring)
print(blub.calc())
print(blub.dump())

blub2 = createJson(teststring)
print(blub2)

# exit()
teststring = "EE00D40C823060"
print(f"Test {teststring} \n11101110000000001101010000001100100000100011000001100000 - expected")
print(hex2bin(teststring))
blub = createPackage(teststring)
print(blub.calc())
print(blub.dump())


teststring = "8A004A801A8002F478"
blub = createPackage(teststring)
print(f"{teststring} (16) - > {blub.versionSum()}")

teststring = "620080001611562C8802118E34"
blub = createPackage(teststring)
print(f"{teststring} (12) - > {blub.versionSum()}")

teststring = "C0015000016115A2E0802F182340"
blub = createPackage(teststring)
print(f"{teststring} (23) - > {blub.versionSum()}")

teststring = "A0016C880162017C3686B18A3D4780"
blub = createPackage(teststring)
print(f"{teststring} (31) - > {blub.versionSum()}")

teststring = "C20D718021600ACDC372CD8DE7A057252A49C940239D68978F7970194EA7CCB310088760088803304A0AC1B100721EC298D3307440041CD8B8005D12DFD27CBEEF27D94A4E9B033006A45FE71D665ACC0259C689B1F99679F717003225900465800804E39CE38CE161007E52F1AEF5EE6EC33600BCC29CFFA3D8291006A92CA7E00B4A8F497E16A675EFB6B0058F2D0BD7AE1371DA34E730F66009443C00A566BFDBE643135FEDF321D000C6269EA66545899739ADEAF0EB6C3A200B6F40179DE31CB7B277392FA1C0A95F6E3983A100993801B800021B0722243D00042E0DC7383D332443004E463295176801F29EDDAA853DBB5508802859F2E9D2A9308924F9F31700AA4F39F720C733A669EC7356AC7D8E85C95E123799D4C44C0109C0AF00427E3CC678873F1E633C4020085E60D340109E3196023006040188C910A3A80021B1763FC620004321B4138E52D75A20096E4718D3E50016B19E0BA802325E858762D1802B28AD401A9880310E61041400043E2AC7E8A4800434DB24A384A4019401C92C154B43595B830002BC497ED9CC27CE686A6A43925B8A9CFFE3A9616E5793447004A4BBB749841500B26C5E6E306899C5B4C70924B77EF254B48688041CD004A726ED3FAECBDB2295AEBD984E08E0065C101812E006380126005A80124048CB010D4C03DC900E16A007200B98E00580091EE004B006902004B00410000AF00015933223100688010985116A311803D05E3CC4B300660BC7283C00081CF26491049F3D690E9802739661E00D400010A8B91F2118803310A2F43396699D533005E37E8023311A4BB9961524A4E2C027EC8C6F5952C2528B333FA4AD386C0A56F39C7DB77200C92801019E799E7B96EC6F8B7558C014977BD00480010D89D106240803518E31C4230052C01786F272FF354C8D4D437DF52BC2C300567066550A2A900427E0084C254739FB8E080111E0"
blub = createPackage(teststring)
print(f"{teststring} (Real Input) - > {blub.versionSum()}")

print("----------------------Puzzle 2 ---------------------")
teststring = "C200B40A82"
blub = createPackage(teststring)
print(f"{teststring} (3) - > {blub.calc()}")

teststring = "04005AC33890"
blub = createPackage(teststring)
print(f"{teststring} (54) - > {blub.calc()}")

teststring = "880086C3E88112"
blub = createPackage(teststring)
print(f"{teststring} (7) - > {blub.calc()}")

teststring = "CE00C43D881120"
blub = createPackage(teststring)
print(f"{teststring} (9) - > {blub.calc()}")

teststring = "D8005AC2A8F0"
blub = createPackage(teststring)
print(f"{teststring} (1) - > {blub.calc()}")

teststring = "F600BC2D8F"
blub = createPackage(teststring)
print(f"{teststring} (0) - > {blub.calc()}")

teststring = "9C005AC2F8F0"
blub = createPackage(teststring)
print(f"{teststring} (0) - > {blub.calc()}")

teststring = "9C0141080250320F1802104A08"
blub = createPackage(teststring)
print(f"{teststring} (1) - > {blub.calc()}")

teststring = "C20D718021600ACDC372CD8DE7A057252A49C940239D68978F7970194EA7CCB310088760088803304A0AC1B100721EC298D3307440041CD8B8005D12DFD27CBEEF27D94A4E9B033006A45FE71D665ACC0259C689B1F99679F717003225900465800804E39CE38CE161007E52F1AEF5EE6EC33600BCC29CFFA3D8291006A92CA7E00B4A8F497E16A675EFB6B0058F2D0BD7AE1371DA34E730F66009443C00A566BFDBE643135FEDF321D000C6269EA66545899739ADEAF0EB6C3A200B6F40179DE31CB7B277392FA1C0A95F6E3983A100993801B800021B0722243D00042E0DC7383D332443004E463295176801F29EDDAA853DBB5508802859F2E9D2A9308924F9F31700AA4F39F720C733A669EC7356AC7D8E85C95E123799D4C44C0109C0AF00427E3CC678873F1E633C4020085E60D340109E3196023006040188C910A3A80021B1763FC620004321B4138E52D75A20096E4718D3E50016B19E0BA802325E858762D1802B28AD401A9880310E61041400043E2AC7E8A4800434DB24A384A4019401C92C154B43595B830002BC497ED9CC27CE686A6A43925B8A9CFFE3A9616E5793447004A4BBB749841500B26C5E6E306899C5B4C70924B77EF254B48688041CD004A726ED3FAECBDB2295AEBD984E08E0065C101812E006380126005A80124048CB010D4C03DC900E16A007200B98E00580091EE004B006902004B00410000AF00015933223100688010985116A311803D05E3CC4B300660BC7283C00081CF26491049F3D690E9802739661E00D400010A8B91F2118803310A2F43396699D533005E37E8023311A4BB9961524A4E2C027EC8C6F5952C2528B333FA4AD386C0A56F39C7DB77200C92801019E799E7B96EC6F8B7558C014977BD00480010D89D106240803518E31C4230052C01786F272FF354C8D4D437DF52BC2C300567066550A2A900427E0084C254739FB8E080111E0"
blub = createPackage(teststring)
print(f"{teststring} (Real Input) - > {blub.calc()}")

blub = createJson(teststring)
print(blub)
# print(blub.dump())
