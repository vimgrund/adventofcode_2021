# tag::parseInput[]
def parseInput(file):
    output = []
    with open(file, 'r') as f:
        while True:
            blub = f.readline().split('|')
            if len(blub) != 2:
                break
            if blub[1][-1] == '\n': # get rid of linebreak
                blub[1] = blub[1][:-1]
            output.append(Line(blub[0].split(' ')[:-1], blub[1][1:].split(' '))) # blub[1][1:] to get rid of leading blank
    return output
# end::parseInput[]

def countUniqueOutputs(outputs):
    counter = 0
    for line in outputs:
        for output in line.output:
            if len(output) == 2 or len(output) == 3 or len(output) == 4 or len(output) == 7:
                counter += 1
    return counter

class Line:
    def __init__(self, pattern, output):
        self.pattern = pattern
        self.output = output
        self.segments = {}
        self.segments['a'] = []
        self.segments['b'] = []
        self.segments['c'] = []
        self.segments['d'] = []
        self.segments['e'] = []
        self.segments['f'] = []
        self.segments['g'] = []

    def determineWires(self):
        self.wires2 = []
        self.wires3 = []
        self.wires4 = []
        self.wires5 = []
        self.wires6 = []
        self.wires7 = []
        for value in self.pattern:
            if len(value) == 2:
                self.wires2.append(value)
            elif len(value) == 3:
                self.wires3.append(value)
            elif len(value) == 4:
                self.wires4.append(value)
            elif len(value) == 5:
                self.wires5.append(value)
            elif len(value) == 6:
                self.wires6.append(value)
            elif len(value) == 7:
                self.wires7.append(value)
            else:
                print(f"ERROR: {value} in pattern")
                exit
        for value in self.output:
            if len(value) == 2:
                self.wires2.append(value)
            elif len(value) == 3:
                self.wires3.append(value)
            elif len(value) == 4:
                self.wires4.append(value)
            elif len(value) == 5:
                self.wires5.append(value)
            elif len(value) == 6:
                self.wires6.append(value)
            elif len(value) == 7:
                self.wires7.append(value)
            else:
                print(f"ERROR: {value} in output")
                exit

        self.filter()
        #print(f"{len(self.wires2)},{len(self.wires3)},{len(self.wires4)},{len(self.wires5)},{len(self.wires6)},{len(self.wires7)}")
        self.diff17()
        self.diff48()
        self.diff14235()
        self.compare235()
        self.compare690()
    def diff17(self):
        seven = self.wires3[0]
        one = self.wires2[0]
        for letter in seven:
            if letter not in one:
                self.segments['a'].append(letter)
            else:
                self.segments['c'].append(letter)
                self.segments['f'].append(letter)
    def diff48(self):
        eight = self.wires7[0]
        four = self.wires4[0]
        for letter in eight:
            if letter not in four and letter not in self.segments['a']:
                self.segments['e'].append(letter)
                self.segments['g'].append(letter)
    def diff14235(self):
        one = self.wires2[0]
        four = self.wires4[0]
        twothreefive = self.wires5
        for letter in four:
            if letter not in one:
                foundB = False
                for random in twothreefive:
                    if letter not in random:
                        foundB = True
                        break
                if foundB:
                    self.segments['b'].append(letter)
                else:
                    self.segments['d'].append(letter)
    def compare235(self):
        for letter in self.wires5[0]:
            if letter not in self.wires5[1] and letter not in self.wires5[2] \
                    and letter not in self.segments['b']:
                self.segments['e'] = [letter]
                blub = []
                for something in self.segments['g']:
                    if something != letter:
                        blub.append(something)
                self.segments['g'] = blub
                return
        for letter in self.wires5[1]:
            if letter not in self.wires5[0] and letter not in self.wires5[2] \
                    and letter not in self.segments['b']:
                self.segments['e'] = [letter]
                blub = []
                for something in self.segments['g']:
                    if something != letter:
                        blub.append(something)
                self.segments['g'] = blub
                return 
        for letter in self.wires5[2]:
            if letter not in self.wires5[0] and letter not in self.wires5[1] \
                    and letter not in self.segments['b']:
                self.segments['e'] = [letter]
                blub = []
                for something in self.segments['g']:
                    if something != letter:
                        blub.append(something)
                self.segments['g'] = blub

    def compare690(self):
        for letter in self.wires6[0]:
            if (letter not in self.wires6[1] or letter not in self.wires6[2]) \
                    and letter in self.wires2[0]:
                self.segments['c'] = [letter]
                blub = []
                for something in self.segments['f']:
                    if something != letter:
                        blub.append(something)
                self.segments['f'] = blub
                return
        for letter in self.wires6[1]:
            if (letter not in self.wires6[0] or letter not in self.wires6[2]) \
                    and letter in self.wires2[0]:
                self.segments['c'] = [letter]
                blub = []
                for something in self.segments['f']:
                    if something != letter:
                        blub.append(something)
                self.segments['f'] = blub
                return 
        for letter in self.wires6[2]:
            if (letter not in self.wires6[0] or letter not in self.wires6[1]) \
                    and letter in self.wires2[0]:
                self.segments['c'] = [letter]
                blub = []
                for something in self.segments['f']:
                    if something != letter:
                        blub.append(something)
                self.segments['f'] = blub







    def filter(self):
        newWire5 = []
        for number in self.wires5:
            clean = []
            for letter in number:
                clean.append(letter)
            clean.sort()
            blub = "".join(clean)
            if blub not in newWire5:
                newWire5.append(blub)
        newWire6 = []
        for number in self.wires6:
            clean = []
            for letter in number:
                clean.append(letter)
            clean.sort()
            blub = "".join(clean)
            if blub not in newWire6:
                newWire6.append(blub)
        self.wires5 = newWire5
        self.wires6 = newWire6
    def decode(self):
        stringresult = ""
        
        for output in self.output:
            segments = ""
            value = ""
            
            for letter in output:
                for segment in self.segments:
                    if letter in self.segments[segment]:
                        segments += segment
            if 'a' in segments and 'b' in segments \
                and 'c' in segments and not 'd' in segments \
                    and 'e' in segments and 'f' in segments \
                        and 'g' in segments:
                value = "0"
            elif not 'a' in segments and not 'b'  in segments \
                and 'c' in segments and not 'd' in segments \
                    and not 'e' in segments and 'f' in segments \
                        and not 'g' in segments:
                value = "1"
            elif 'a' in segments and not 'b' in segments \
                and 'c' in segments  and 'd' in segments \
                    and 'e' in segments and not 'f' in segments \
                        and 'g' in segments:
                value = "2"
            elif 'a' in segments and not 'b' in segments \
                and 'c' in segments and 'd' in segments \
                    and not 'e' in segments and 'f' in segments \
                        and 'g' in segments:
                value = "3"
            elif not 'a' in segments and 'b' in segments \
                and 'c' in segments and 'd' in segments \
                    and not 'e' in segments and 'f' in segments \
                        and not 'g' in segments:
                value = "4"
            elif 'a' in segments and 'b' in segments \
                and not 'c' in segments and 'd' in segments \
                    and not 'e' in segments and 'f' in segments \
                        and 'g' in segments:
                value = "5"
            elif 'a' in segments and 'b' in segments \
                and not 'c' in segments and 'd' in segments \
                    and 'e' in segments and 'f' in segments \
                        and 'g' in segments:
                value = "6"
            elif 'a' in segments and not 'b'  in segments \
                and 'c' in segments and not 'd' in segments \
                    and not 'e' in segments and 'f' in segments \
                        and not 'g' in segments:
                value = "7"
            elif 'a' in segments and 'b' in segments \
                and 'c' in segments and 'd' in segments \
                    and 'e' in segments and 'f' in segments and 'g' in segments:
                value = "8"
            elif 'a' in segments and 'b' in segments \
                and 'c' in segments and 'd' in segments \
                    and not 'e' in segments and 'f' in segments \
                        and 'g' in segments:
                value = "9"
            if value == "":
                print("EERRORROR")
                exit()
            stringresult += value
        result = -1
        # check segments for completion
        return int(stringresult)

print("----------------Puzzle 1--------------")

# tag::example1[]
test_lines = parseInput("test.txt")
print("Test input, should be 26")
blub = countUniqueOutputs(test_lines)
print(blub)
# end::example1[]

lines = parseInput("input.txt")
print("Real Input")
blub = countUniqueOutputs(lines)
print(blub)

print("----------------Puzzle 2--------------")

# tag::example1[]
single_test_line = parseInput("test_single.txt")
print("Test input, should be 5353")
blub = 0
for line in single_test_line:
    line.determineWires()
    blub += line.decode()
print(blub)
# end::example1[]

print("Test input, should be 26")
blub = 0
for line in test_lines:
    line.determineWires()
    blub += line.decode()
print(blub)

print("Real Input")
blub = 0
for line in lines:
    line.determineWires()
    blub += line.decode()
print(blub)
