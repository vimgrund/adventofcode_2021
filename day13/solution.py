# tag::parseInput[]
def parseInput(file):
    with open(file, 'r') as f:
        dots = []
        folding = False
        instructions = []
        max_x = 0
        max_y = 0
        while True:
            line = f.readline()
            if line == "":
                break
            if line == "\n":
                folding = True
                continue
            if line[-1] == '\n':
                line = line[:-1]

            if folding == False: 
                blub = line.split(',')
                x = int(blub[0])
                y = int(blub[1])
                dots.append(Dot(x, y))
                max_x = getMax(max_x, x)
                max_y = getMax(max_y, y)
            else:
                blub = line.split('=')
                axis = blub[0].split(' ')[-1]
                instructions.append(Instruction(axis, int(blub[1])))
        map = Map(dots, instructions, max_x, max_y)
    return map
# end::parseInput[]

def getMax(*values):
    result = None
    for value in values:
        if result is None or value > result:
            result = value
    return result

class Dot:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Instruction:
    def __init__(self, axis, value):
        self.axis = axis
        self.value = value

class Map:
    def __init__(self, dots, instructions, max_x, max_y):
        self.dots = dots
        self.instructions = instructions
        self.max_x = max_x
        self.max_y = max_y

    def fold(self):
        if len(self.instructions) == 0:
            # we are done folding
            return False

        instruction = self.instructions[0]
        self.instructions = self.instructions[1:] # remove instruction
        if instruction.axis == 'y':
            for dot in self.dots:
                if dot.y >= instruction.value:
                    dot.y = instruction.value - (dot.y - instruction.value)
            self.max_y = int(self.max_y / 2)
        else:
            for dot in self.dots:
                if dot.x >= instruction.value:
                    dot.x = instruction.value - (dot.x - instruction.value)
            self.max_x = int(self.max_x / 2)
        return True

    def print(self):
        rows = []
        for i in range(0, self.max_y+1):
            rows.append([0]*(self.max_x+1))
        for dot in self.dots:
            rows[dot.y][dot.x] = 1

        counter = 0
        for row in rows:
            output = ""
            for digit in row:
                if digit == 1:
                    output += '#'
                    counter += 1
                else:
                    output += ' '
            print(output, flush=True)
        return counter

print("----------------Puzzle 1--------------")

# tag::example1[]
test_map = parseInput("test.txt")
print("Test input, should be 17")
test_map.fold()
blub = test_map.print()
print(blub)
# end::example1[]

map = parseInput("input.txt")
print("Real Input")
map.fold()
blub = map.print()
print(blub)


print("----------------Puzzle 2--------------")

# tag::example2[]
print("Test input, should look like 0")
while (test_map.fold()):
    pass
blub = test_map.print()
print(blub)
# end::example2[]

print("Real Input")
while map.fold():
    pass
blub = map.print()
print(blub)
